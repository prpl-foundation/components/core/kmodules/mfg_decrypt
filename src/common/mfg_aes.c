/****************************************************************************
**
** SPDX-License-Identifier: Dual BSD-2-Clause-Patent AND GPL-2.0-or-later
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** **BSD 2-Clause Patent License**
**
** 1. Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with the distribution.
**
** **GPL 2.0 or later**
**
** Subject to the terms and conditions of the GPL 2.0 or later, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor are granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * @file
 * @brief AES Encryption and Decryption Functions
 *
 * This file provides functions for AES encryption and decryption in the MFG (Manufacturing) module.
 * The encryption algorithm used is AES-128 in CBC mode with Base64 encoding.
 */

#include <linux/version.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
#include <linux/crypto.h>
#else
#include <crypto/skcipher.h>
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */
#include <linux/err.h>
#include <linux/scatterlist.h>
#include <linux/random.h>
#include <linux/printk.h>

#define MFG_PRIV_KEY_ENCFORMAT_AES128CBC_B64    "aes-128-cbc-base64"
#define MFG_PRIV_KEY_KEYFORMAT_AES128EKIV       "aes-128-ek-iv" /* once decrypted, EK = 1st 128b and IV = last 128b */

#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
static const char blkcipher_alg[] = "cbc(aes)";
#else
static const char skcipher_alg[] = "cbc(aes)";
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */

/**
 * @defgroup mfg_aes MFG AES Encryption/Decryption
 * @{
 */

/**
 * @ingroup mfg_aes
 * @brief Initialize a block cipher descriptor for AES encryption.
 *
 * @param desc      Pointer to the block cipher descriptor to be initialized.
 * @param key       Pointer to the encryption key.
 * @param key_len   Length of the encryption key.
 * @param iv        Pointer to the initialization vector.
 * @param ivsize    Size of the initialization vector.
 *
 * @return 0 on success, a negative error code on failure.
 */
#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
static int init_blkcipher_desc(struct blkcipher_desc* desc, const u8* key,
                               unsigned int key_len, const u8* iv,
                               unsigned int ivsize) {
    int ret = 0;

    BUG_ON(NULL == desc);

    memset(desc, 0, sizeof(struct blkcipher_desc));

    desc->tfm = crypto_alloc_blkcipher(blkcipher_alg, 0, 0);
    if(IS_ERR(desc->tfm)) {
        pr_err("encrypted_key: failed to load %s transform (%ld)\n",
               blkcipher_alg, PTR_ERR(desc->tfm));
        return PTR_ERR(desc->tfm);
    }
    desc->flags = 0;

    ret = crypto_blkcipher_setkey(desc->tfm, key, key_len);
    if(ret < 0) {
        pr_err("encrypted_key: failed to setkey (%d)\n", ret);
        goto error;
    }

    if(ivsize) {
        if(ivsize != crypto_blkcipher_ivsize(desc->tfm)) {
            pr_err("IV length differs from expected length\n");
            ret = -1;
            goto error;
        }
        crypto_blkcipher_set_iv(desc->tfm, iv, ivsize);
    } else {
        ret = -1;
        goto error;
    }

    return ret;

error:
    crypto_free_blkcipher(desc->tfm);
    return ret;
}
#else /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */
static struct skcipher_request* init_skcipher_req(const u8* key,
                                                  unsigned int key_len) {
    struct skcipher_request* req;
    struct crypto_skcipher* tfm;
    int ret;

    tfm = crypto_alloc_skcipher(skcipher_alg, 0, CRYPTO_ALG_ASYNC);
    if(IS_ERR(tfm)) {
        pr_err("encrypted_key: failed to load %s transform (%ld)\n",
               skcipher_alg, PTR_ERR(tfm));
        return ERR_CAST(tfm);
    }

    ret = crypto_skcipher_setkey(tfm, key, key_len);
    if(ret < 0) {
        pr_err("encrypted_key: failed to setkey (%d)\n", ret);
        crypto_free_skcipher(tfm);
        return ERR_PTR(ret);
    }

    req = skcipher_request_alloc(tfm, GFP_KERNEL);
    if(!req) {
        pr_err("encrypted_key: failed to allocate request for %s\n",
               skcipher_alg);
        crypto_free_skcipher(tfm);
        return ERR_PTR(-ENOMEM);
    }

    skcipher_request_set_callback(req, 0, NULL, NULL);
    return req;
}
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */

/**
 * @ingroup mfg_aes
 * @brief Perform AES encryption on the provided data.
 *
 * @param data            Pointer to the data to be encrypted.
 * @param length          Length of the data to be encrypted.
 * @param encrypted_data  Pointer to store the encrypted data.
 * @param key             Encryption key.
 * @param key_size        Size of the encryption key.
 * @param iv              Initialization vector.
 * @param iv_size         Size of the initialization vector.
 *
 * @return The length of the encrypted data on success, a negative error code on failure.
 */
int mfg_aes_encrypt(const unsigned char* data, unsigned int length, unsigned char** encrypted_data, const char* key, unsigned int key_size, const char* iv, unsigned int iv_size) {
#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
    struct blkcipher_desc desc = {0};
#else
    struct crypto_skcipher* tfm;
    struct skcipher_request* req;
    void* iv_tmp;
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */
    struct scatterlist sg_in[2];
    struct scatterlist sg_out[1];
    char pad[16] = {0};
    unsigned int encrypted_datalen, padlen;
    int ret = 0;

    if((NULL == data) || (NULL == key) || (NULL == iv)) {
        ret = -EFAULT;
        goto out;
    }

#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
    ret = init_blkcipher_desc(&desc, key, key_size, iv, iv_size);
    if(ret < 0) {
        goto out;
    }

#else
    req = init_skcipher_req(key, key_size);
    ret = PTR_ERR(req);
    if(IS_ERR(req)) {
        goto out;
    }
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */

#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
    encrypted_datalen = roundup(length, crypto_blkcipher_blocksize(desc.tfm));
#else
    tfm = crypto_skcipher_reqtfm(req);
    encrypted_datalen = roundup(length, crypto_skcipher_blocksize(tfm));
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */

    padlen = encrypted_datalen - length;

    //pr_err("encrypted_datalen: %d - padlen: %d\n", encrypted_datalen, padlen);

    *encrypted_data = kzalloc(encrypted_datalen, GFP_KERNEL);
    if(NULL == *encrypted_data) {
        pr_err("%s: Failed allocating memory!\n", __FUNCTION__);
        ret = -ENOMEM;
        goto out;
    }

    sg_init_table(sg_in, 2);
    sg_set_buf(&sg_in[0], data, length);
    sg_set_buf(&sg_in[1], pad, padlen);

    sg_init_table(sg_out, 1);
    sg_set_buf(sg_out, *encrypted_data, encrypted_datalen);

#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
    ret = crypto_blkcipher_encrypt(&desc, sg_out, sg_in, encrypted_datalen);
#else
    /*
     * save the iv
     */
    iv_tmp = kmalloc(iv_size, GFP_KERNEL);
    if(!iv_tmp) {
        pr_err("%s: can't allocate iv_tmp\n", __func__);
        goto out;
    }
    memcpy(iv_tmp, iv, iv_size);

    skcipher_request_set_crypt(req, sg_in, sg_out, encrypted_datalen, (void*) iv_tmp);
    ret = crypto_skcipher_encrypt(req);
    tfm = crypto_skcipher_reqtfm(req);

    kfree(iv_tmp);
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */

    if(ret < 0) {
        pr_err("%s: failed to encrypt (%d)\n", __FUNCTION__, ret);
        kfree(*encrypted_data);
        *encrypted_data = NULL;
    } else {
        //print_hex_dump(KERN_ERR, "Encrypted data: ", DUMP_PREFIX_NONE, 32, 1,
        //               *encrypted_data, encrypted_datalen, 0);
        ret = encrypted_datalen;
    }

out:
#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
    if(desc.tfm) {
        crypto_free_blkcipher(desc.tfm);
    }
#else
    if(!IS_ERR(req)) {
        skcipher_request_free(req);
        crypto_free_skcipher(tfm);
    }
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */

    return ret;
}

/**
 * @ingroup mfg_aes
 * @brief Perform AES decryption on the provided data.
 *
 * @param data            Pointer to the data to be decrypted.
 * @param length          Length of the data to be decrypted.
 * @param decrypted_data  Pointer to store the decrypted data.
 * @param key             Decryption key.
 * @param key_size        Size of the decryption key.
 * @param iv              Initialization vector.
 * @param iv_size         Size of the initialization vector.
 *
 * @return The length of the decrypted data on success, a negative error code on failure.
 */
int mfg_aes_decrypt(const unsigned char* data, unsigned int length, unsigned char** decrypted_data, const char* key, unsigned int key_size, const char* iv, unsigned int iv_size) {
#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
    struct blkcipher_desc desc = {0};
#else
    struct crypto_skcipher* tfm;
    struct skcipher_request* req;
    void* iv_tmp;
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */
    struct scatterlist sg_in[1];
    struct scatterlist sg_out[1];
    unsigned int encrypted_datalen;
    int ret = 0;

    if((NULL == data) || (NULL == decrypted_data) || (NULL == key) || (NULL == iv)) {
        ret = -EINVAL;
        goto out;
    }

#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
    ret = init_blkcipher_desc(&desc, key, key_size, iv, iv_size);
    if(ret < 0) {
        goto out;
    }
#else
    req = init_skcipher_req(key, key_size);
    ret = PTR_ERR(req);
    if(IS_ERR(req)) {
        goto out;
    }
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */


#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
    if(length % crypto_blkcipher_blocksize(desc.tfm)) {
        pr_err("%s: Invalid length (%u)\n", __FUNCTION__, length);
        ret = -EINVAL;
        goto out;
    }
#else
    tfm = crypto_skcipher_reqtfm(req);

    if(length % crypto_skcipher_blocksize(tfm)) {
        pr_err("%s: Invalid length (%u)\n", __FUNCTION__, length);
        ret = -EINVAL;
        goto out;
    }
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */

    /* For now, it should be at most as long as the encrypted length */
    /* And it will be resize after decryption */
    encrypted_datalen = length;
    *decrypted_data = kzalloc(encrypted_datalen, GFP_KERNEL);
    if(NULL == *decrypted_data) {
        pr_err("%s: Failed allocating memory!\n", __FUNCTION__);
        ret = -ENOMEM;
        goto out;
    }

    sg_init_table(sg_in, 1);
    sg_init_table(sg_out, 1);
    sg_set_buf(sg_in, data, encrypted_datalen);
    sg_set_buf(sg_out, *decrypted_data, encrypted_datalen);

#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
    ret = crypto_blkcipher_decrypt(&desc, sg_out, sg_in, encrypted_datalen);
#else
    /*
     * save the iv
     */
    iv_tmp = kmalloc(iv_size, GFP_KERNEL);
    if(!iv_tmp) {
        pr_err("%s: can't allocate iv_tmp\n", __func__);
        goto out;
    }
    memcpy(iv_tmp, iv, iv_size);

    skcipher_request_set_crypt(req, sg_in, sg_out, encrypted_datalen, iv_tmp);
    ret = crypto_skcipher_decrypt(req);
    tfm = crypto_skcipher_reqtfm(req);

    kfree(iv_tmp);
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */

    if(ret < 0) {
        pr_err("%s: failed to decrypt (%d)\n", __FUNCTION__, ret);
        kfree(*decrypted_data);
        goto out;
    } else {
        unsigned int i;
        unsigned int padding_bytes = (*decrypted_data)[encrypted_datalen - 1];
        unsigned int decrypted_datalen = 0;

        //pr_err("%s: encrypted_datalen=%u - padding_bytes=%u\n",
        //       __FUNCTION__, encrypted_datalen, padding_bytes);

        //print_hex_dump(KERN_ERR, "Encrypted data: ", DUMP_PREFIX_NONE, 32, 1,
        //               *decrypted_data, encrypted_datalen, 0);

        if(padding_bytes > encrypted_datalen) {
            pr_warn("%s: Padding bytes (%d bytes) are bigger than the encrypted data (%d bytes)\n", __FUNCTION__, padding_bytes, encrypted_datalen);
            pr_warn("%s: Key or iv mismatch?\n", __FUNCTION__);
            /* Just provide the data without truncation */
            decrypted_datalen = encrypted_datalen;
        } else {
            /* Check padding bytes consistencies: they must all be equal */
            for(i = 1; i < padding_bytes; ++i) {
                if((*decrypted_data)[encrypted_datalen - i] != (*decrypted_data)[encrypted_datalen - i - 1]) {
                    break;
                }
            }

            if(i < padding_bytes) {
                pr_warn("%s: Padding bytes are not equal\n", __FUNCTION__);
                pr_warn("%s: Key or iv mismatch?\n", __FUNCTION__);
                /* Just provide the data without truncation */
                decrypted_datalen = encrypted_datalen;
            } else {
                /* Now we can safely remove the padding byte(s) */
                if(padding_bytes == encrypted_datalen) { /* Empty decrypted data */
                    kfree(*decrypted_data);

                    decrypted_datalen = 1;

                    *decrypted_data = kzalloc(decrypted_datalen, GFP_KERNEL);
                    if(NULL == *decrypted_data) {
                        pr_err("%s: Failed allocating memory!\n", __FUNCTION__);
                        ret = -ENOMEM;
                        goto out;
                    }
                    **decrypted_data = '\0'; /* Empty string */
                } else {
                    char* buf = *decrypted_data;

                    decrypted_datalen = encrypted_datalen - padding_bytes;

                    *decrypted_data = kzalloc(decrypted_datalen, GFP_KERNEL);
                    if(NULL == *decrypted_data) {
                        pr_err("%s: Failed allocating memory!\n", __FUNCTION__);
                        kfree(buf);
                        ret = -ENOMEM;
                        goto out;
                    }
                    memcpy(*decrypted_data, buf, decrypted_datalen);
                    kfree(buf);
                }
            }
        }
        ret = decrypted_datalen;
    }

out:
#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0)
    if(desc.tfm) {
        crypto_free_blkcipher(desc.tfm);
    }
#else
    if(!IS_ERR(req)) {
        skcipher_request_free(req);
        crypto_free_skcipher(tfm);
    }
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(4, 10, 0) */

    return ret;
}

/**
 * @}
 */
