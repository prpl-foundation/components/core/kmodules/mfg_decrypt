/****************************************************************************
**
** SPDX-License-Identifier: Dual BSD-2-Clause-Patent AND GPL-2.0-or-later
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** **BSD 2-Clause Patent License**
**
** 1. Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with the distribution.
**
** **GPL 2.0 or later**
**
** Subject to the terms and conditions of the GPL 2.0 or later, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor are granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * @file
 * @brief Shared functions for all versions.
 *
 * This file defines shared functions that are used in the different versions of the mfgdecrypt kernel module.
 */

#include "shared_functions.h"


/**
 * @defgroup shared_functions that can be used by the modules
 * @{
 */

/**
 * @ingroup shared_functions
 * @brief MFG Data Clear Keys
 *
 * Clears the keys stored in the MFG Data structure by freeing the allocated memory.
 * @param mfgData The struct wich contains mfg_drvdata.
 */
void mfg_data_clear_keys(struct mfg_drvdata* mfgData) {
    if(NULL != mfgData->mfgKey) {
        DEBUG("MFG: free existing key\n");
        memset(mfgData->mfgKey, 0, mfgData->mfgKey_size);
        kfree(mfgData->mfgKey);
        mfgData->mfgKey = NULL;
        mfgData->mfgKey_size = 0;
    }

    if(NULL != mfgData->mfgIv) {
        DEBUG("MFG: free existing IV\n");
        memset(mfgData->mfgIv, 0, mfgData->mfgIv_size);
        kfree(mfgData->mfgIv);
        mfgData->mfgIv = NULL;
        mfgData->mfgIv_size = 0;
    }
}

/**
 * @ingroup shared_functions
 * @brief MFG Data Allocate Keys
 *
 * Allocates memory to store keys in the MFG Data structure. The buffer is appended
 * with an extra byte for the NULL character.
 *
 * In case of an error, both keys are cleared.
 *
 * @param mfgData The struct wich contains mfg_drvdata.
 * @param mfg_key_size The size of the AES MFG key.
 * @param mfg_iv_size The size of the IV MFG key.
 *
 * @return 0 on success, 1 on error.
 */
int mfg_data_alloc_keys(struct mfg_drvdata* mfgData, int mfg_key_size, int mfg_iv_size) {
    mfgData->mfgKey_size = mfg_key_size;
    mfgData->mfgKey = kzalloc(mfg_key_size + 1, GFP_KERNEL);
    if(NULL == mfgData->mfgKey) {
        pr_err("MFG: error allocating memory for key\n");
        return -ENOMEM;
    }

    mfgData->mfgIv_size = mfg_iv_size;
    mfgData->mfgIv = kzalloc(mfg_iv_size + 1, GFP_KERNEL);
    if(NULL == mfgData->mfgIv) {
        pr_err("MFG: error allocating memory for IV\n");
        mfg_data_clear_keys(mfgData);
        return -ENOMEM;
    }

    return 0;
}

/**
 * @}
 */
