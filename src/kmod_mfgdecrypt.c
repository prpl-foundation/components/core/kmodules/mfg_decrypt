/****************************************************************************
**
** SPDX-License-Identifier: Dual BSD-2-Clause-Patent AND GPL-2.0-or-later
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** **BSD 2-Clause Patent License**
**
** 1. Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with the distribution.
**
** **GPL 2.0 or later**
**
** Subject to the terms and conditions of the GPL 2.0 or later, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor are granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * @file kmod_mfgdecrypt.c
 * @brief MFG Decryption Module Implementation
 *
 * This file contains the implementation of the MFG (Manufacturing) Decrypt module.
 * The module is responsible for initializing, handling IOCTL commands, and cleaning up
 * resources associated with the MFG Decryption functionality. It includes functions for
 * decoding base64 data, managing MFG keys, and decrypting the MFG key used in manufacturing.
 *
 * The MFG Decryption module supports dynamic allocation of memory for keys and uses IOCTL
 * commands to perform decryption operations on MFG data. Additionally, the module provides
 * support for reading the MFG key from the kernel device tree or a secure monitor call
 * based on configuration.
 *
 * This module will get the key for decoding the field passed from the bootloader.
 *
 * @version 2.0
 */
#include <linux/version.h>
#include <linux/types.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/of.h>
#include <linux/proc_fs.h>
#include <linux/platform_device.h>
#include <linux/fs.h>
#include <asm/io.h>
#include <linux/of_address.h>
#include <linux/libfdt.h>

#include "base64.h"
#include "mfg_decrypt.h"
#include "shared_functions.h"

#ifndef CONFIG_OF
#error "MFGDECRYPT driver requires CONFIG_OF to be set (FDT support)"
#endif

/** Uncomment to enable debug information */
// #define MFG_DEBUG


#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 5, 0)
static inline void inode_lock(struct inode* inode) {
    mutex_lock(&inode->i_mutex);
}

static inline void inode_unlock(struct inode* inode) {
    mutex_unlock(&inode->i_mutex);
}
#endif

/**
 * @defgroup MFG_MODULE MFG Decryption Module
 * @{
 */

static struct mfg_drvdata mfgData = {0};

extern int mfg_aes_encrypt(const unsigned char* data, unsigned int length, unsigned char** encrypted_data, const char* key, unsigned int key_size, const char* iv, unsigned int iv_size);
extern int mfg_aes_decrypt(const unsigned char* data, unsigned int length, unsigned char** decrypted_data, const char* key, unsigned int key_size, const char* iv, unsigned int iv_size);

/**
 * @ingroup MFG_MODULE
 * @brief MFG IOCTL Handler
 *
 * Handles IOCTL commands for MFG decryption. It decrypts the MFG data based on the
 * provided information in the IOCTL request.
 *
 * @param file File structure pointer.
 * @param cmd IOCTL command.
 * @param arg IOCTL arguments.
 *
 * @return 0 on success, an error code on failure.
 */
static long mfg_ioctl(struct file* file, unsigned int cmd, unsigned long arg) {
    struct inode* inode = NULL;
    mfg_decrypt_info_t info = {0};
    mfg_decrypt_info_t __user* info_u = (mfg_decrypt_info_t __user*) arg;
    u8 __user* encrypted_data = NULL;
    u8 __user* decrypted_data = NULL;
    u8 __user* cipherkey_name = NULL;
    unsigned char* encrypted_databin = NULL;
    int encrypted_databinlen = 0;
    char* keynode_path = NULL;
    const u8* key = NULL, * iv = NULL;
    u32 keylen, ivlen;
    int ret = 0;

    if(file == NULL) {
        pr_err("The passed file to mfg_ioctl is NULL");
        return -EFAULT;
    }
    inode = file->f_path.dentry->d_inode;

    DEBUG("%s: cmd(%u)\n", __FUNCTION__, cmd);

    switch(cmd) {
    case MFG_DECRYPT:
        inode_lock(inode);
        if(copy_from_user(&info, info_u, sizeof(info))) {
            pr_err("%s: copy_from_user failed\n", __FUNCTION__);
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
        /* Backup pointers to userspace for later use */
        encrypted_data = info.encrypted_data;
        decrypted_data = info.decrypted_data;
        cipherkey_name = info.cipherkey_name;
        /* Remove all references of userspace addresses in kernelspace info structure */
        info.encrypted_data = info.decrypted_data = info.cipherkey_name = NULL;
        /* Copy userspace encrypted data to kernelspace */
        info.encrypted_data = kzalloc(info.encrypted_datalen, GFP_KERNEL);
        if(NULL == info.encrypted_data) {
            pr_err("%s: Failed allocating memory!\n", __FUNCTION__);
            ret = -ENOMEM;
            inode_unlock(inode);
            break;
        }
        if(copy_from_user(info.encrypted_data, encrypted_data, info.encrypted_datalen)) {
            pr_err("%s: copy_from_user failed\n", __FUNCTION__);
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
        DEBUG("%s: info.encrypted_datalen=%u\n", __FUNCTION__,
              info.encrypted_datalen);
        DEBUG("%s: Encrypted data (base64): %.*s\n", __FUNCTION__,
              info.encrypted_datalen, info.encrypted_data);
        /* Copy userspace cipher key name to kernelspace */
        if(info.cipherkey_namelen > 0) {
            info.cipherkey_name = kzalloc(info.cipherkey_namelen, GFP_KERNEL);
            if(NULL == info.cipherkey_name) {
                pr_err("%s: Failed allocating memory!\n", __FUNCTION__);
                ret = -ENOMEM;
                inode_unlock(inode);
                break;
            }
            if(copy_from_user(info.cipherkey_name, cipherkey_name, info.cipherkey_namelen)) {
                pr_err("%s: copy_from_user failed\n", __FUNCTION__);
                ret = -EFAULT;
                inode_unlock(inode);
                break;
            }
            DEBUG("%s: info.cipherkey_name=%s\n", __FUNCTION__, info.cipherkey_name);
        }
        encrypted_databin = base64_decode(info.encrypted_data, info.encrypted_datalen,
                                          &encrypted_databinlen);
        DEBUG("%s: encrypted_databinlen=%d\n", __FUNCTION__, encrypted_databinlen);
        if(NULL == encrypted_databin) {
            pr_err("%s: Failed base64 decoding\n", __FUNCTION__);
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
#ifdef MFG_DEBUG
        print_hex_dump(KERN_ERR, "Encrypted databin: ", DUMP_PREFIX_NONE, 32, 1,
                       encrypted_databin, encrypted_databinlen, 0);
#endif

        if(mfgData.fdt) {
            char root_node[] = "/skeys/symmetric-key/";
            int len = info.cipherkey_namelen + strlen(root_node) + 1;
            int offset = 0;

            keynode_path = kzalloc(len, GFP_KERNEL);
            if(NULL == keynode_path) {
                pr_err("%s: Failed allocating memory!\n", __FUNCTION__);
                ret = -ENOMEM;
                inode_unlock(inode);
                break;
            }
            snprintf(keynode_path, len, "%s%s", root_node, info.cipherkey_name);

            if((offset = fdt_path_offset(mfgData.fdt, keynode_path)) < 0) {
                pr_err("%s: Failed locating node: %s\n", __FUNCTION__, keynode_path);
                ret = offset;
                inode_unlock(inode);
                break;
            }
            key = fdt_getprop(mfgData.fdt, offset, "key", &len);
            if((NULL == key) || (len < 0)) {
                pr_err("Could not retrieve key property from node %s\n", keynode_path);
                ret = len;
                inode_unlock(inode);
                break;
            }
            keylen = (u32) len;
            iv = fdt_getprop(mfgData.fdt, offset, "iv", &len);
            if((NULL == iv) || (len < 0)) {
                pr_err("Could not retrieve iv property from node %s\n", keynode_path);
                ret = len;
                inode_unlock(inode);
                break;
            }
            ivlen = (u32) len;
        } else {
            key = mfgData.mfgKey;
            keylen = mfgData.mfgKey_size;
            iv = mfgData.mfgIv;
            ivlen = mfgData.mfgIv_size;
        }

        ret = mfg_aes_decrypt(encrypted_databin, encrypted_databinlen,
                              (unsigned char**) &info.decrypted_data,
                              key, keylen, iv, ivlen);
        if(ret < 0) {
            pr_err("%s: Failed decrypting the MFG data\n", __FUNCTION__);
            inode_unlock(inode);
            break;
        } else if(ret > info.decrypted_datalen) {
            pr_err("%s: Provided buffer is too small. Length=%d\n",
                   __FUNCTION__, ret);
            ret = -EINVAL;
            inode_unlock(inode);
            break;
        }
        info.decrypted_datalen = ret;
        DEBUG("%s: Decrypted data length: %d\n", __FUNCTION__, info.decrypted_datalen);
#ifdef MFG_DEBUG
        print_hex_dump(KERN_ERR, "Decrypted data: ", DUMP_PREFIX_NONE, 32, 1,
                       info.decrypted_data, info.decrypted_datalen, 0);
#endif
        /* Copy kernel space decrypted_data to userspace decrypted_data */
        if(copy_to_user(decrypted_data, info.decrypted_data,
                        info.decrypted_datalen)) {
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
        /* Free kernelspace memory before loosing track of those pointers */
        kfree(info.encrypted_data);
        kfree(info.decrypted_data);
        kfree(info.cipherkey_name);
        /* Set back pointers in info to userspace pointers before copying info to info_u */
        info.decrypted_data = decrypted_data;
        info.encrypted_data = encrypted_data;
        info.cipherkey_name = cipherkey_name;
        if(copy_to_user(info_u, &info, sizeof(info))) {
            info.decrypted_data = info.encrypted_data = info.cipherkey_name = NULL;
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
        /* Nullified those pointers since they now point to userspace memory  */
        info.decrypted_data = info.encrypted_data = info.cipherkey_name = NULL;
        inode_unlock(inode);
        break;
    default:
        pr_err("%s: Unknown command (%u)\n", __FUNCTION__, cmd);
        ret = -EINVAL;
    }
    kfree(encrypted_databin);
    kfree(info.encrypted_data);
    kfree(info.decrypted_data);
    kfree(info.cipherkey_name);
    kfree(keynode_path);

    return ret;
}

static struct file_operations mfg_fops = {
    .owner = THIS_MODULE,
    .unlocked_ioctl = mfg_ioctl,
#ifdef CONFIG_COMPAT
    .compat_ioctl = mfg_ioctl,
#endif
};

/**
 * @ingroup MFG_MODULE
 * @brief Function called when a platform device is probed
 *
 * This function is called by the kernel when a platform device matching
 * the driver's compatible string is found. It initializes any necessary
 * resources and registers the device with the kernel. The @pdev argument
 * provides information about the platform device being probed.
 *
 * @param pdev Pointer to the platform device structure
 *
 * @return 0 on success, negative error code on failure.
 */
static int mfg_probe(struct platform_device* pdev) {
    int rc = 0;
    struct device_node* np;
    struct resource res;
    resource_size_t mem_start; /* phys. address in RAM where the MFG key is stored */
    resource_size_t mem_end;   /* phys. address in RAM where the MFG key is stored */
    resource_size_t mem_size;
    void* base_address;        /* virt. address to access the MFG key */
    const __be32* mfg_key_offs, * mfg_key_size, * mfg_iv_offs, * mfg_iv_size;
    const void* fdt = NULL;

    /* Get reserved memory region from Device-tree */
    np = of_parse_phandle(pdev->dev.of_node, "memory-region", 0);
    if(NULL == np) {
        dev_err(&pdev->dev, "No memory-region specified => Can't retrieve MFG key\n");
        return -EINVAL;
    }

    rc = of_address_to_resource(np, 0, &res);
    if(rc) {
        dev_err(&pdev->dev, "No memory address assigned to the region => Can't retrieve MFG key\n");
        return rc;
    }

    mem_start = res.start;
    mem_end = res.end;
    mem_size = resource_size(&res);

    DEBUG("mem_start=0x%llx - mem_end=0x%llx - mem_size=0x%llx\n",
          mem_start, mem_end, mem_size);

    if(!request_mem_region(mem_start,
                           mem_size, DRIVER_NAME)) {
        dev_err(&pdev->dev, "Couldn't lock memory region at %Lx\n",
                (unsigned long long) mem_start);
        return -EBUSY;
    }

    /* Call ioremap_wc() as we map System RAM */
    base_address = (void*) ioremap_wc(mem_start, mem_size);
    if(NULL == base_address) {
        dev_err(&pdev->dev, "ioremap_wc() failed\n");
        release_mem_region(mem_start, mem_size);
        return -ENOMEM;
    } else if(!fdt_check_header(base_address)) {
        pr_info("Device tree detected\n");
        fdt = base_address;
#ifdef MFG_DEBUG
        print_hex_dump(KERN_ERR, "skeys: ", DUMP_PREFIX_NONE, 32, 1,
                       fdt, fdt_totalsize(fdt), 0);
#endif
    }

    if(fdt) { /* Copy the device tree for later use */
        if(NULL == (mfgData.fdt = kmalloc(fdt_totalsize(fdt), GFP_KERNEL))) {
            dev_err(&pdev->dev, "kmalloc failed to allocate fdt buffer (%d bytes)\n", fdt_totalsize(fdt));
            iounmap(base_address);
            release_mem_region(mem_start, mem_size);
            return -ENOMEM;
        }
        memcpy(mfgData.fdt, fdt, fdt_totalsize(fdt));
        //TODO: In the case the keys are stored in a device-tree, the reserved memory is not zeroed
        //      as this FDT could contain other keys used by other drivers. This is not really safe
        //      but ultimately the keys should be stored in SRAM and handled by OPTEE.
    } else { /* A single key and IV is stored in memory */
        mfg_key_offs = of_get_property(pdev->dev.of_node, "key_offset", NULL);
        mfg_key_size = of_get_property(pdev->dev.of_node, "key_size", NULL);
        mfg_iv_offs = of_get_property(pdev->dev.of_node, "iv_offset", NULL);
        mfg_iv_size = of_get_property(pdev->dev.of_node, "iv_size", NULL);
        if((NULL == mfg_key_offs) || (NULL == mfg_key_size) ||
           (NULL == mfg_iv_offs) || (NULL == mfg_iv_size)) {
            dev_err(&pdev->dev, "Missing key and/or iv information => Can't retrieve MFG key\n");
            iounmap(base_address);
            release_mem_region(mem_start, mem_size);
            return -EINVAL;
        }

        mfgData.mfgKey_size = be32_to_cpu(*mfg_key_size);
        mfgData.mfgIv_size = be32_to_cpu(*mfg_iv_size);

        DEBUG("mfgKey_offs=0x%x - mfgKey_size=0x%x - mfgIv_offs=0x%x - mfgIv_size=0x%x\n",
              be32_to_cpu(*mfg_key_offs), mfgData.mfgKey_size, be32_to_cpu(*mfg_iv_offs), mfgData.mfgIv_size);

        /* Retrieve the mfg key and IV */
        if(NULL == (mfgData.mfgKey = kmalloc(mfgData.mfgKey_size, GFP_KERNEL))) {
            dev_err(&pdev->dev, "kmalloc failed allocated mfgKey buffer (%d bytes)\n", mfgData.mfgKey_size);
            iounmap(base_address);
            release_mem_region(mem_start, mem_size);
            return -ENOMEM;
        }
        if(NULL == (mfgData.mfgIv = kmalloc(mfgData.mfgIv_size, GFP_KERNEL))) {
            dev_err(&pdev->dev, "kmalloc failed allocated mfgIv buffer (%d bytes)\n", mfgData.mfgIv_size);
            kfree(mfgData.mfgKey);
            iounmap(base_address);
            release_mem_region(mem_start, mem_size);
            return -ENOMEM;
        }
        memcpy(mfgData.mfgKey, base_address + be32_to_cpu(*mfg_key_offs), mfgData.mfgKey_size);
        memcpy(mfgData.mfgIv, base_address + be32_to_cpu(*mfg_iv_offs), mfgData.mfgIv_size);

#ifdef MFG_DEBUG
        print_hex_dump(KERN_ERR, "MFG key: ", DUMP_PREFIX_NONE, 32, 1,
                       mfgData.mfgKey, mfgData.mfgKey_size, 0);
        print_hex_dump(KERN_ERR, "MFG IV: ", DUMP_PREFIX_NONE, 32, 1,
                       mfgData.mfgIv, mfgData.mfgIv_size, 0);
#endif

        /* For security reasons, the reserved memory is zeroed to avoid any
         * other kernel code to request and map that memory region where the
         * MFG key is stored */
        memset(base_address, 0, mem_size);
    }
    /* Unmap and release the memory region */
    iounmap(base_address);
    release_mem_region(mem_start, mem_size);

    cdev_init(&mfgData.cdev, &mfg_fops);
    mfgData.cdev.owner = THIS_MODULE;
    rc = cdev_add(&mfgData.cdev, mfgData.devt, DEVICE_NUMBER);
    if(rc) {
        dev_err(&pdev->dev, "cdev_add() failed\n");
        kfree(mfgData.mfgIv);
        kfree(mfgData.mfgKey);
        kfree(mfgData.fdt);
        return rc;
    }

    mfgData.mfgDevice = device_create(mfgData.mfgClass, &pdev->dev,
                                      mfgData.devt, NULL, DEVICE_NAME);
    if(IS_ERR(mfgData.mfgDevice)) {
        dev_err(&pdev->dev, "Failed to create the device\n");
        cdev_del(&mfgData.cdev);
        kfree(mfgData.mfgIv);
        kfree(mfgData.mfgKey);
        kfree(mfgData.fdt);
        return PTR_ERR(mfgData.mfgDevice);
    }

    return 0;
}

/**
 * @ingroup MFG_MODULE
 * @brief Function called when a platform device is removed
 *
 * This function is called by the kernel when a platform device previously
 * probed by the driver is being removed from the system. It performs any
 * necessary cleanup operations, releases allocated resources, and unregisters
 * the device from the kernel.
 *
 * @param pdev argument provides information about the platform device being removed.
 *
 * @return 0
 */
static int mfg_remove(struct platform_device* pdev) {
    device_destroy(mfgData.mfgClass, mfgData.devt);
    cdev_del(&mfgData.cdev);
    kfree(mfgData.mfgIv);
    kfree(mfgData.mfgKey);
    kfree(mfgData.fdt);

    return 0;
}

/**
 * @ingroup MFG_MODULE
 * @brief mfg_of_match - Array of device ID structures for device-tree matching
 *
 * This array contains device ID structures used for matching devices
 * based on their device tree (DT) compatible string. Each structure
 * specifies a compatible string that the driver supports.
 *
 * The array must be terminated with an empty entry (an entry with
 * all fields set to zero). This indicates the end of the list.
 *
 * Structure members:
 * - compatible: A string specifying the compatible string to match
 *               devices against. This string is typically defined
 *               in the device tree nodes of compatible devices.
 */
static const struct of_device_id mfg_of_match[] = {
    {
        .compatible = "mfgdecrypt",
    },
    {},
};
MODULE_DEVICE_TABLE(of, mfg_of_match);

/**
 * @ingroup MFG_MODULE
 * @brief mfg_driver - Platform driver structure for the MFG driver
 *
 * This structure defines the platform driver for the MFG driver.
 * It specifies callback functions for device probing and removal,
 * as well as driver-specific information such as the driver name and
 * device tree (DT) matching table.
 *
 * Structure members:
 * - probe: Pointer to the function called when a matching platform device
 *          is found and needs to be probed.
 * - remove: Pointer to the function called when a platform device previously
 *           probed by the driver is being removed from the system.
 * - driver: Structure containing driver-specific information.
 *   - name: A string specifying the name of the driver.
 *   - of_match_table: Pointer to the device tree (DT) matching table.
 */
static struct platform_driver mfg_driver = {
    .probe = mfg_probe,
    .remove = mfg_remove,
    .driver = {
        .name = DRIVER_NAME,
        .of_match_table = of_match_ptr(mfg_of_match),
    }
};

/**
 * @ingroup MFG_MODULE
 * @brief MFG Decryption Module Initialization
 *
 * This function is called when the MFG (Manufacturing) Decryption module is initialized.
 * It performs initialization tasks such as creating a device class, allocating
 * character device region, and probing the platform driver. If any initialization
 * step fails, appropriate error messages are printed, and cleanup is performed
 * before returning an error code.
 *
 * @return 0 on success, an error code on failure.
 */
static int __init mfg_init(void) {
    int retval = 0;

    mfgData.mfgClass = class_create(THIS_MODULE, CLASS_NAME);
    if(IS_ERR(mfgData.mfgClass)) {
        pr_alert("Failed to register device class\n");
        return PTR_ERR(mfgData.mfgClass);
    }

    retval = alloc_chrdev_region(&mfgData.devt, MFG_MINOR, DEVICE_NUMBER, DEVICE_NAME);
    if(retval < 0) {
        pr_alert("MFG: alloc_chrdev_region failed (0x%x)\n", retval);
        class_destroy(mfgData.mfgClass);
        return retval;
    }

    retval = platform_driver_probe(&mfg_driver, mfg_probe);
    if(0 != retval) {
        pr_err("MFG: failed to init mfg key from the bootloader\n");
        unregister_chrdev_region(mfgData.devt, DEVICE_NUMBER);
        class_destroy(mfgData.mfgClass);
        return retval;
    }

    return retval;
}

/**
 * @ingroup MFG_MODULE
 * @brief MFG Decryption Module Exit
 *
 * This function is called when the MFG (Manufacturing) Decryption module is being
 * unloaded from the kernel. It performs cleanup tasks such as unregistering
 * the platform driver, releasing the character device region, and clearing
 * any allocated keys or resources associated with the MFG data.
 */
static void __exit mfg_exit(void) {
    platform_driver_unregister(&mfg_driver);
    unregister_chrdev_region(mfgData.devt, DEVICE_NUMBER);
    mfg_data_clear_keys(&mfgData);

    pr_info("MFG: unregister SoftAtHome MFG driver\n");
}

module_init(mfg_init);
module_exit(mfg_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jonathan Luijsmans");
MODULE_AUTHOR("Paul HENRYS");
MODULE_DESCRIPTION("SoftAtHome driver to decipher MFG secured data");
MODULE_VERSION("1.0");

/**
 * @}
 */
