/****************************************************************************
**
** SPDX-License-Identifier: Dual BSD-2-Clause-Patent AND GPL-2.0-or-later
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** **BSD 2-Clause Patent License**
**
** 1. Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with the distribution.
**
** **GPL 2.0 or later**
**
** Subject to the terms and conditions of the GPL 2.0 or later, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor are granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * @file kmod_mfgdecrypt_key_in_dtb.c
 * @brief MFG Decryption Module Implementation
 *
 * This file contains the implementation of the MFG (Manufacturing) Decrypt module.
 * The module is responsible for initializing, handling IOCTL commands, and cleaning up
 * resources associated with the MFG Decryption functionality. It includes functions for
 * decoding base64 data, managing MFG keys, and decrypting the MFG key used in manufacturing.
 *
 * The MFG Decryption module supports dynamic allocation of memory for keys and uses IOCTL
 * commands to perform decryption operations on MFG data. Additionally, the module provides
 * support for reading the MFG key from the kernel device tree or a secure monitor call
 * based on configuration.
 *
 * This module retrieves the key to decrypt the field from the MFG itself.
 * This is achieved by passing the encrypted RIP_KEY as an argument when initializing the module.
 *
 * @version 1.0
 */
#include <linux/version.h>
#include <linux/types.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/of.h>
#include <linux/proc_fs.h>
#include <linux/platform_device.h>
#include <linux/of_address.h>

#include "base64.h"
#include "mfg_decrypt.h"
#include "shared_functions.h"

#include <asm/io.h>
#include <asm-generic/io.h>
#include <linux/arm-smccc.h>

/**
 * @defgroup MFG_MODULE_key_in_mfg MFG Decryption Module Getting the key from the MFG
 * @{
 */

/** Secure monitor SMC ID: fast call, SMC32, SiP service call. */
#define SMC_ID          0x82000206
/**
 * The physical address the encrypted rip key is written to. The decrypted rip
 * key can be read from this address after decryption.
 * */
static resource_size_t mem_start; /* phys. address in RAM where the MFG key is stored */
static resource_size_t mem_end;   /* phys. address in RAM where the MFG key is stored */
static resource_size_t mem_size;


/** Uncomment to enable debug information */
// #define MFG_DEBUG

#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 5, 0)
static inline void inode_lock(struct inode* inode) {
    mutex_lock(&inode->i_mutex);
}

static inline void inode_unlock(struct inode* inode) {
    mutex_unlock(&inode->i_mutex);
}
#endif

static struct mfg_drvdata mfgData = {0};

static char* rip_key;
module_param(rip_key, charp, 0644);
MODULE_PARM_DESC(rip_key, "rip_key");

extern int mfg_aes_encrypt(const unsigned char* data, unsigned int length, unsigned char** encrypted_data, const char* key, unsigned int key_size, const char* iv, unsigned int iv_size);
extern int mfg_aes_decrypt(const unsigned char* data, unsigned int length, unsigned char** decrypted_data, const char* key, unsigned int key_size, const char* iv, unsigned int iv_size);

/**
 * @ingroup MFG_MODULE_key_in_mfg
 * @brief MFG IOCTL Handler
 *
 * Handles IOCTL commands for MFG decryption. It decrypts the MFG data based on the
 * provided information in the IOCTL request.
 *
 * @param file File structure pointer.
 * @param cmd IOCTL command.
 * @param arg IOCTL arguments.
 *
 * @return 0 on success, an error code on failure.
 */
static long mfg_ioctl(struct file* file, unsigned int cmd, unsigned long arg) {
    struct inode* inode = NULL;
    mfg_decrypt_info_t info = {0};
    mfg_decrypt_info_t __user* info_u = (mfg_decrypt_info_t __user*) arg;
    u8 __user* encrypted_data = NULL;
    u8 __user* decrypted_data = NULL;
    u8 __user* cipherkey_name = NULL;
    unsigned char* encrypted_databin = NULL;
    int encrypted_databinlen = 0;
    char* keynode_path = NULL;
    const u8* key = NULL, * iv = NULL;
    u32 keylen, ivlen;
    int ret = 0;

    if(file == NULL) {
        pr_err("The passed file to mfg_ioctl is NULL");
        return -EFAULT;
    }
    inode = file->f_path.dentry->d_inode;

    DEBUG("%s: cmd(%u)\n", __FUNCTION__, cmd);

    switch(cmd) {
    case MFG_DECRYPT:
        inode_lock(inode);
        if(copy_from_user(&info, info_u, sizeof(info))) {
            pr_err("%s: copy_from_user failed\n", __FUNCTION__);
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
        /* Backup pointers to userspace for later use */
        encrypted_data = info.encrypted_data;
        decrypted_data = info.decrypted_data;
        cipherkey_name = info.cipherkey_name;
        /* Remove all references of userspace addresses in kernelspace info structure */
        info.encrypted_data = info.decrypted_data = info.cipherkey_name = NULL;
        /* Copy userspace encrypted data to kernelspace */
        info.encrypted_data = kzalloc(info.encrypted_datalen, GFP_KERNEL);
        if(NULL == info.encrypted_data) {
            pr_err("%s: Failed allocating memory!\n", __FUNCTION__);
            ret = -ENOMEM;
            inode_unlock(inode);
            break;
        }
        if(copy_from_user(info.encrypted_data, encrypted_data, info.encrypted_datalen)) {
            pr_err("%s: copy_from_user failed\n", __FUNCTION__);
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
        DEBUG("%s: info.encrypted_datalen=%u\n", __FUNCTION__,
              info.encrypted_datalen);
        DEBUG("%s: Encrypted data (base64): %.*s\n", __FUNCTION__,
              info.encrypted_datalen, info.encrypted_data);
        /* Copy userspace cipher key name to kernelspace */
        if(info.cipherkey_namelen > 0) {
            info.cipherkey_name = kzalloc(info.cipherkey_namelen, GFP_KERNEL);
            if(NULL == info.cipherkey_name) {
                pr_err("%s: Failed allocating memory!\n", __FUNCTION__);
                ret = -ENOMEM;
                inode_unlock(inode);
                break;
            }
            if(copy_from_user(info.cipherkey_name, cipherkey_name, info.cipherkey_namelen)) {
                pr_err("%s: copy_from_user failed\n", __FUNCTION__);
                ret = -EFAULT;
                inode_unlock(inode);
                break;
            }
            DEBUG("%s: info.cipherkey_name=%s\n", __FUNCTION__, info.cipherkey_name);
        }
        encrypted_databin = base64_decode(info.encrypted_data, info.encrypted_datalen,
                                          &encrypted_databinlen);
        DEBUG("%s: encrypted_databinlen=%d\n", __FUNCTION__, encrypted_databinlen);
        if(NULL == encrypted_databin) {
            pr_err("%s: Failed base64 decoding\n", __FUNCTION__);
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
#ifdef MFG_DEBUG
        print_hex_dump(KERN_ERR, "Encrypted databin: ", DUMP_PREFIX_NONE, 32, 1,
                       encrypted_databin, encrypted_databinlen, 0);
#endif

        key = mfgData.mfgKey;
        keylen = mfgData.mfgKey_size;
        iv = mfgData.mfgIv;
        ivlen = mfgData.mfgIv_size;

        ret = mfg_aes_decrypt(encrypted_databin, encrypted_databinlen,
                              (unsigned char**) &info.decrypted_data,
                              key, keylen, iv, ivlen);
        if(ret < 0) {
            pr_err("%s: Failed decrypting the MFG data\n", __FUNCTION__);
            inode_unlock(inode);
            break;
        } else if(ret > info.decrypted_datalen) {
            pr_err("%s: Provided buffer is too small. Length=%d\n",
                   __FUNCTION__, ret);
            ret = -EINVAL;
            inode_unlock(inode);
            break;
        }
        info.decrypted_datalen = ret;
        DEBUG("%s: Decrypted data length: %d\n", __FUNCTION__, info.decrypted_datalen);
#ifdef MFG_DEBUG
        print_hex_dump(KERN_ERR, "Decrypted data: ", DUMP_PREFIX_NONE, 32, 1,
                       info.decrypted_data, info.decrypted_datalen, 0);
#endif
        /* Copy kernel space decrypted_data to userspace decrypted_data */
        if(copy_to_user(decrypted_data, info.decrypted_data,
                        info.decrypted_datalen)) {
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
        /* Free kernelspace memory before loosing track of those pointers */
        kfree(info.encrypted_data);
        kfree(info.decrypted_data);
        kfree(info.cipherkey_name);
        /* Set back pointers in info to userspace pointers before copying info to info_u */
        info.decrypted_data = decrypted_data;
        info.encrypted_data = encrypted_data;
        info.cipherkey_name = cipherkey_name;
        if(copy_to_user(info_u, &info, sizeof(info))) {
            info.decrypted_data = info.encrypted_data = info.cipherkey_name = NULL;
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
        /* Nullified those pointers since they now point to userspace memory  */
        info.decrypted_data = info.encrypted_data = info.cipherkey_name = NULL;
        inode_unlock(inode);
        break;
    default:
        pr_err("%s: Unknown command (%u)\n", __FUNCTION__, cmd);
        ret = -EINVAL;
    }
    kfree(encrypted_databin);
    kfree(info.encrypted_data);
    kfree(info.decrypted_data);
    kfree(info.cipherkey_name);
    kfree(keynode_path);

    return ret;
}

static struct file_operations mfg_fops = {
    .owner = THIS_MODULE,
    .unlocked_ioctl = mfg_ioctl,
#ifdef CONFIG_COMPAT
    .compat_ioctl = mfg_ioctl,
#endif
};

/**
 * @ingroup MFG_MODULE_key_in_mfg
 * @brief MFG Get SMC Virtual Address
 *
 * Gets the virtual address where the RIP key data is written to or read from
 * by the secure monitor.
 *
 * @return Virtual address.
 */
static inline void* mfg_get_smc_virtual_address(void) {
    return phys_to_virt((phys_addr_t) mem_start);
}

/**
 * @ingroup MFG_MODULE_key_in_mfg
 * @brief MFG Encode UTF-8 to Hex
 *
 * Hex encodes a UTF-8 string.
 *
 * @param utf8 Pointer to a NULL-terminated UTF-8 string.
 * @param hex Pointer to the address of a char buffer to store the encoded hex data.
 * @param hex_length The length of the allocated char buffer pointed to by hex.
 *
 * @return 0 on success, 1 on error.
 */
static int mfg_encode_utf8_to_hex(const char* utf8, char* hex, size_t hex_length) {
    size_t i = 0;
    unsigned int* hex_array = (unsigned int*) hex;
    char hex_digit[9] = {0};
    unsigned long res = 0;

    for(i = 0; i < (hex_length / sizeof(*hex_array)); i++) {
        strncpy(hex_digit, utf8 + i * 8, 8);
        res = kstrtouint(hex_digit, 16, &hex_array[i]);
        if(res < 0) {
            pr_err("MFG: failed to encode UTF-8 character\n");
            return 1;
        }
        hex_array[i] = __builtin_bswap32(hex_array[i]);
    }

#ifdef MFG_DEBUG
    DEBUG("MFG: hex encoded rip_key (size = %d): ", hex_length);

    for(i = 0; i < hex_length; i++) {
        DEBUG("%x", hex[i]);
    }

    DEBUG("\n");
#endif

    return 0;
}

/**
 * @ingroup MFG_MODULE_key_in_mfg
 * @brief MFG Validate RIP Key Parameter
 *
 * Validates the provided 'rip_key' parameter. The rip_key must not be NULL
 * and must have the correct size.
 *
 * @return 1 when valid, otherwise 0.
 */
static int mfg_rip_key_param_is_valid(void) {
    if(NULL == rip_key) {
        pr_err("MFG: error: rip_key must be set\n");
        return 0;
    }

    DEBUG("rip_key (size = %d): %s\n", strlen(rip_key), rip_key);

    if(strlen(rip_key) != ENC_RIP_KEY_IV_TAG_SIZE_UTF8) {
        pr_err("MFG: error: invalid rip_key\n");
        return 0;
    }

    return 1;
}

/**
 * @ingroup MFG_MODULE_key_in_mfg
 * @brief MFG Prepare Decrypt RIP Key
 *
 * Prepares the 'rip_key' parameter to be decrypted. Converts the 'rip_key'
 * from UTF-8 to hex encoding and writes the hex encoded key to memory
 * used by the secure monitor for decryption.
 *
 * @return 0 on success, 1 on error.
 */
static int mfg_prepare_decrypt_rip_key(void) {
    char rip_key_hex_encoded[ENC_RIP_KEY_IV_TAG_SIZE + 1] = {0};
    void* smc_key_virt_addr = NULL;

    if(0 != mfg_encode_utf8_to_hex(rip_key, rip_key_hex_encoded, ENC_RIP_KEY_IV_TAG_SIZE)) {
        pr_err("MFG: failed to encode rip_key\n");
        return 1;
    }

    smc_key_virt_addr = mfg_get_smc_virtual_address();
    if(NULL == smc_key_virt_addr) {
        pr_err("MFG: failed to get key address\n");
        return 1;
    }

    memcpy(smc_key_virt_addr, rip_key_hex_encoded, ENC_RIP_KEY_IV_TAG_SIZE);

    DEBUG("MFG: %d bytes written to 0x%p for decryption\n", ENC_RIP_KEY_IV_TAG_SIZE, smc_key_virt_addr);

    return 0;
}

/**
 * @ingroup MFG_MODULE_key_in_mfg
 * @brief MFG Decrypt RIP Key
 *
 * Decrypts the RIP key using a secure monitor call.
 *
 * @return Result of the secure monitor call.
 */
static unsigned long mfg_decrypt_rip_key(void) {
    unsigned long r0 = 0, r1 = 0, r2 = 0, r3 = 0;
    struct arm_smccc_res res;
    r0 = SMC_ID;
    r1 = mem_start;
    DEBUG("mfgdecrypt: The address used in the smc call is: %x\n", r1);

    arm_smccc_smc(r0, r1, r2, r3, 0, 0, 0, 0, &res);

    if(0 != res.a0) {
        pr_err("MFG: failed to decrypt rip_key\n");
    }

    return res.a0;
}

/**
 * @ingroup MFG_MODULE_key_in_mfg
 * @brief MFG Store Decrypted RIP Key
 *
 * Stores the decrypted RIP key on the heap in mfg_drvdata pointed to by mfgData.
 * The decrypted and encrypted key at SMC_KEY_PHYS_ADDR/mem_start is erased after the copy.
 * When a decrypted key is already allocated, the key is freed.
 *
 * @return 0 on success, 1 on error.
 */
static int mfg_store_decrypted_rip_key(void) {
    void* smc_key_virt_addr = NULL;
#ifdef MFG_DEBUG
    int i = 0;
#endif

    smc_key_virt_addr = mfg_get_smc_virtual_address();
    if(NULL == smc_key_virt_addr) {
        pr_err("MFG: failed to get key address\n");
        return 1;
    }

    mfg_data_clear_keys(&mfgData);

    if(0 != mfg_data_alloc_keys(&mfgData, DEC_RIP_KEY_SIZE, DEC_RIP_IV_SIZE)) {
        pr_err("MFG: failed to alloc keys\n");
        return 1;
    }

    memcpy(mfgData.mfgKey, smc_key_virt_addr, DEC_RIP_KEY_SIZE);
    memcpy(mfgData.mfgIv, smc_key_virt_addr + DEC_RIP_KEY_SIZE, DEC_RIP_IV_SIZE);
    memset(smc_key_virt_addr, 0, ENC_RIP_KEY_IV_TAG_SIZE);

#ifdef MFG_DEBUG
    DEBUG("MFG: decrypted rip key (size = %d): ", DEC_RIP_KEY_IV_SIZE);

    for(i = 0; i < DEC_RIP_KEY_SIZE; i++) {
        DEBUG("%02x", mfgData.mfgKey[i]);
    }
    for(i = 0; i < DEC_RIP_IV_SIZE; i++) {
        DEBUG("%02x", mfgData.mfgIv[i]);
    }

    DEBUG("\n");
#endif

    return 0;
}

/**
 * @ingroup MFG_MODULE_key_in_mfg
 * @brief MFG RIP Key Initialization
 *
 * Initializes the RIP key by validating, preparing, decrypting, and storing it.
 *
 * @return 0 on success, an error code on failure.
 */
static int mfg_rip_key_init(void) {

    if(0 == mfg_rip_key_param_is_valid()) {
        return -EINVAL;
    }

    if(0 != mfg_prepare_decrypt_rip_key()) {
        return -EFAULT;
    }

    if(0 != mfg_decrypt_rip_key()) {
        return -EFAULT;
    }

    if(0 != mfg_store_decrypted_rip_key()) {
        return -EFAULT;
    }

    return 0;
}

/**
 * @ingroup MFG_MODULE
 * @brief Function called when a platform device is probed
 *
 * This function is called by the kernel when a platform device matching
 * the driver's compatible string is found. It initializes any necessary
 * resources and registers the device with the kernel. The @pdev argument
 * provides information about the platform device being probed.
 *
 * @param pdev Pointer to the platform device structure
 *
 * @return 0 on success, negative error code on failure.
 */
static int mfg_probe(struct platform_device* pdev) {
    int rc = 0;
    int retval = 0;
    struct device_node* np;
    struct resource res;

    /* Get reserved memory region from Device-tree */
    np = of_parse_phandle(pdev->dev.of_node, "memory-region", 0);
    if(NULL == np) {
        dev_err(&pdev->dev, "No memory-region specified => Can't retrieve MFG key reserved memory\n");
        return -EINVAL;
    }

    rc = of_address_to_resource(np, 0, &res);
    if(rc) {
        dev_err(&pdev->dev, "No memory address assigned to the region => Can't retrieve MFG key reserved memory\n");
        return rc;
    }

    mem_start = res.start;
    mem_end = res.end;
    mem_size = resource_size(&res);

    DEBUG("mem_start=0x%llx - mem_end=0x%llx - mem_size=0x%llx\n",
          mem_start, mem_end, mem_size);

    if(!request_mem_region(mem_start,
                           mem_size, DRIVER_NAME)) {
        dev_err(&pdev->dev, "Couldn't lock memory region at %Lx\n",
                (unsigned long long) mem_start);
        return -EBUSY;
    }

    // Obtain the decrypted rip key
    retval = mfg_rip_key_init();
    if(0 != retval) {
        pr_err("MFG: failed to init rip key\n");
        return retval;
    }

    /* Unmap and release the memory region */
    release_mem_region(mem_start, mem_size);

    /* Char device registration. Initialize and add the character device. */
    cdev_init(&mfgData.cdev, &mfg_fops);
    mfgData.cdev.owner = THIS_MODULE;
    mfgData.cdev.ops = &mfg_fops;
    retval = cdev_add(&mfgData.cdev, mfgData.devt, DEVICE_NUMBER);
    if(retval) {
        pr_err("MFG: failed to add character device\n");
        mfg_data_clear_keys(&mfgData);
        return retval;
    }

    /* Create /dev/mfg for this char dev. */
    mfgData.mfgDevice = device_create(mfgData.mfgClass, NULL, mfgData.devt, NULL, DEVICE_NAME);
    if(IS_ERR(mfgData.mfgDevice)) {
        printk(KERN_ERR "mfgdecrypt: Failed to create device\n");
        mfg_data_clear_keys(&mfgData);
        cdev_del(&mfgData.cdev);
        return PTR_ERR(mfgData.mfgDevice);
    }

    return 0;
}

/**
 * @ingroup MFG_MODULE
 * @brief Function called when a platform device is removed
 *
 * This function is called by the kernel when a platform device previously
 * probed by the driver is being removed from the system. It performs any
 * necessary cleanup operations, releases allocated resources, and unregisters
 * the device from the kernel.
 *
 * @param pdev argument provides information about the platform device being removed.
 *
 * @return 0
 */
static int mfg_remove(struct platform_device* pdev) {
    device_destroy(mfgData.mfgClass, mfgData.devt);
    cdev_del(&mfgData.cdev);
    mfg_data_clear_keys(&mfgData);

    return 0;
}

/**
 * @ingroup MFG_MODULE
 * @brief mfg_of_match - Array of device ID structures for device-tree matching
 *
 * This array contains device ID structures used for matching devices
 * based on their device tree (DT) compatible string. Each structure
 * specifies a compatible string that the driver supports.
 *
 * The array must be terminated with an empty entry (an entry with
 * all fields set to zero). This indicates the end of the list.
 *
 * Structure members:
 * - compatible: A string specifying the compatible string to match
 *               devices against. This string is typically defined
 *               in the device tree nodes of compatible devices.
 */
static const struct of_device_id mfg_of_match[] = {
    {
        .compatible = "mfgdecrypt",
    },
    {},
};
MODULE_DEVICE_TABLE(of, mfg_of_match);

/**
 * @ingroup MFG_MODULE
 * @brief mfg_driver - Platform driver structure for the MFG driver
 *
 * This structure defines the platform driver for the MFG driver.
 * It specifies callback functions for device probing and removal,
 * as well as driver-specific information such as the driver name and
 * device tree (DT) matching table.
 *
 * Structure members:
 * - probe: Pointer to the function called when a matching platform device
 *          is found and needs to be probed.
 * - remove: Pointer to the function called when a platform device previously
 *           probed by the driver is being removed from the system.
 * - driver: Structure containing driver-specific information.
 *   - name: A string specifying the name of the driver.
 *   - of_match_table: Pointer to the device tree (DT) matching table.
 */
static struct platform_driver mfg_driver = {
    .remove = mfg_remove,
    .driver = {
        .name = DRIVER_NAME,
        .of_match_table = of_match_ptr(mfg_of_match),
    }
};

/*
 * When loading the module via `modprobe mfgdecrypt rip_key=$(readmfg -s RIP_KEY)`,
 * the module decrypts the given RIP_KEY, which will be stored in a newly
 * allocated buffer in kernel space. The decrypted key in the buffer is
 * used to decipher the encrypted MFG fields.
 */
/**
 * @ingroup MFG_MODULE_key_in_mfg
 * @brief MFG Decryption Module Initialization
 *
 * This function initializes the MFG Decryption module. It performs the following steps:
 * - Retrieve and set the decryption key.
 * - Prepares and decrypts the RIP key.
 * - Allocates the major number for the device.
 * - Creates the device class and character device.
 *
 * @return 0 on success, an error code on failure.
 */
static int __init mfg_init(void) {
    int retval = 0;

    /* Allocate the MAJOR number of the device. */
    retval = alloc_chrdev_region(&mfgData.devt, MFG_MINOR, DEVICE_NUMBER, DEVICE_NAME);
    if(retval < 0) {
        pr_err("MFG: alloc_chrdev_region failed (0x%x)\n", retval);
        return -ENOMEM;
    }

    /* Create /sys/class/mfg in preparation of /dev/mfg */
    mfgData.mfgClass = class_create(THIS_MODULE, CLASS_NAME);
    if(IS_ERR(mfgData.mfgClass)) {
        pr_err("MFG: Failed to create device class\n");
        unregister_chrdev_region(mfgData.devt, DEVICE_NUMBER);
        return PTR_ERR(mfgData.mfgClass);
    }

    retval = platform_driver_probe(&mfg_driver, mfg_probe);
    if(0 != retval) {
        pr_err("MFG: failed to init RIP_KEY in memory\n");
        unregister_chrdev_region(mfgData.devt, DEVICE_NUMBER);
        class_destroy(mfgData.mfgClass);
        return retval;
    }

    pr_info("MFG: SoftAtHome MFG driver has been initialized (major=%d)\n", MAJOR(mfgData.devt));
    return retval;
}

/**
 * @ingroup MFG_MODULE_key_in_mfg
 * @brief MFG Decryption Module Exit
 *
 * This function cleans up and exits the MFG Decryption module. It destroys the device,
 * character device, and class. Additionally, it unregisters the allocated major number.
 */
static void __exit mfg_exit(void) {
    platform_driver_unregister(&mfg_driver);
    class_unregister(mfgData.mfgClass);
    class_destroy(mfgData.mfgClass);
    unregister_chrdev_region(mfgData.devt, DEVICE_NUMBER);

    pr_info("MFG: unregister SoftAtHome MFG driver\n");
}

module_init(mfg_init);
module_exit(mfg_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jonathan Luijsmans");
MODULE_AUTHOR("Paul HENRYS");
MODULE_DESCRIPTION("SoftAtHome driver to decipher MFG secured data. The key comes from the MFG itself");
MODULE_VERSION("1.0");

/**
 * @}
 */
