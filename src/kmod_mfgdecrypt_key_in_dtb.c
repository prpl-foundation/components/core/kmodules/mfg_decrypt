/****************************************************************************
**
** SPDX-License-Identifier: Dual BSD-2-Clause-Patent AND GPL-2.0-or-later
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** **BSD 2-Clause Patent License**
**
** 1. Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with the distribution.
**
** **GPL 2.0 or later**
**
** Subject to the terms and conditions of the GPL 2.0 or later, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor are granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * @file kmod_mfgdecrypt_key_in_dtb.c
 * @brief MFG Decryption Module Implementation
 *
 * This file contains the implementation of the MFG (Manufacturing) Decrypt module.
 * The module is responsible for initializing, handling IOCTL commands, and cleaning up
 * resources associated with the MFG Decryption functionality. It includes functions for
 * decoding base64 data, managing MFG keys, and decrypting the MFG key used in manufacturing.
 *
 * The MFG Decryption module supports dynamic allocation of memory for keys and uses IOCTL
 * commands to perform decryption operations on MFG data. Additionally, the module provides
 * support for reading the MFG key from the kernel device tree or a secure monitor call
 * based on configuration.
 *
 * This module retrieves the key to decrypt the field from the kernel dtb.
 * This is achieved by reading the cipher key from the kernel dtb.
 *
 * @version 1.0
 */
#include <linux/version.h>
#include <linux/types.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/of.h>
#include <linux/proc_fs.h>

#include "base64.h"
#include "mfg_decrypt.h"
#include "shared_functions.h"

/** Uncomment to enable debug information */
// #define MFG_DEBUG

#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 5, 0)
static inline void inode_lock(struct inode* inode) {
    mutex_lock(&inode->i_mutex);
}

static inline void inode_unlock(struct inode* inode) {
    mutex_unlock(&inode->i_mutex);
}
#endif

/**
 * @defgroup MFG_MODULE_key_in_dtb MFG Decryption Module getting the key from the kernel dtb
 * @{
 */

static struct mfg_drvdata mfgData = {0};

extern int mfg_aes_encrypt(const unsigned char* data, unsigned int length, unsigned char** encrypted_data, const char* key, unsigned int key_size, const char* iv, unsigned int iv_size);
extern int mfg_aes_decrypt(const unsigned char* data, unsigned int length, unsigned char** decrypted_data, const char* key, unsigned int key_size, const char* iv, unsigned int iv_size);

/**
 * @ingroup MFG_MODULE_key_in_dtb
 * @brief MFG IOCTL Handler
 *
 * Handles IOCTL commands for MFG decryption. It decrypts the MFG data based on the
 * provided information in the IOCTL request.
 *
 * @param file File structure pointer.
 * @param cmd IOCTL command.
 * @param arg IOCTL arguments.
 *
 * @return 0 on success, an error code on failure.
 */
static long mfg_ioctl(struct file* file, unsigned int cmd, unsigned long arg) {
    struct inode* inode = NULL;
    mfg_decrypt_info_t info = {0};
    mfg_decrypt_info_t __user* info_u = (mfg_decrypt_info_t __user*) arg;
    u8 __user* encrypted_data = NULL;
    u8 __user* decrypted_data = NULL;
    u8 __user* cipherkey_name = NULL;
    unsigned char* encrypted_databin = NULL;
    int encrypted_databinlen = 0;
    char* keynode_path = NULL;
    const u8* key = NULL, * iv = NULL;
    u32 keylen, ivlen;
    int ret = 0;

    if(file == NULL) {
        pr_err("The passed file to mfg_ioctl is NULL");
        return -EFAULT;
    }
    inode = file->f_path.dentry->d_inode;

    DEBUG("%s: cmd(%u)\n", __FUNCTION__, cmd);

    switch(cmd) {
    case MFG_DECRYPT:
        inode_lock(inode);
        if(copy_from_user(&info, info_u, sizeof(info))) {
            pr_err("%s: copy_from_user failed\n", __FUNCTION__);
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
        /* Backup pointers to userspace for later use */
        encrypted_data = info.encrypted_data;
        decrypted_data = info.decrypted_data;
        cipherkey_name = info.cipherkey_name;
        /* Remove all references of userspace addresses in kernelspace info structure */
        info.encrypted_data = info.decrypted_data = info.cipherkey_name = NULL;
        /* Copy userspace encrypted data to kernelspace */
        info.encrypted_data = kzalloc(info.encrypted_datalen, GFP_KERNEL);
        if(NULL == info.encrypted_data) {
            pr_err("%s: Failed allocating memory!\n", __FUNCTION__);
            ret = -ENOMEM;
            inode_unlock(inode);
            break;
        }
        if(copy_from_user(info.encrypted_data, encrypted_data, info.encrypted_datalen)) {
            pr_err("%s: copy_from_user failed\n", __FUNCTION__);
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
        DEBUG("%s: info.encrypted_datalen=%u\n", __FUNCTION__,
              info.encrypted_datalen);
        DEBUG("%s: Encrypted data (base64): %.*s\n", __FUNCTION__,
              info.encrypted_datalen, info.encrypted_data);
        /* Copy userspace cipher key name to kernelspace */
        if(info.cipherkey_namelen > 0) {
            info.cipherkey_name = kzalloc(info.cipherkey_namelen, GFP_KERNEL);
            if(NULL == info.cipherkey_name) {
                pr_err("%s: Failed allocating memory!\n", __FUNCTION__);
                ret = -ENOMEM;
                inode_unlock(inode);
                break;
            }
            if(copy_from_user(info.cipherkey_name, cipherkey_name, info.cipherkey_namelen)) {
                pr_err("%s: copy_from_user failed\n", __FUNCTION__);
                ret = -EFAULT;
                inode_unlock(inode);
                break;
            }
            DEBUG("%s: info.cipherkey_name=%s\n", __FUNCTION__, info.cipherkey_name);
        }
        encrypted_databin = base64_decode(info.encrypted_data, info.encrypted_datalen,
                                          &encrypted_databinlen);
        DEBUG("%s: encrypted_databinlen=%d\n", __FUNCTION__, encrypted_databinlen);
        if(NULL == encrypted_databin) {
            pr_err("%s: Failed base64 decoding\n", __FUNCTION__);
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
#ifdef MFG_DEBUG
        print_hex_dump(KERN_ERR, "Encrypted databin: ", DUMP_PREFIX_NONE, 32, 1,
                       encrypted_databin, encrypted_databinlen, 0);
#endif

        key = mfgData.mfgKey;
        keylen = mfgData.mfgKey_size;
        iv = mfgData.mfgIv;
        ivlen = mfgData.mfgIv_size;

        ret = mfg_aes_decrypt(encrypted_databin, encrypted_databinlen,
                              (unsigned char**) &info.decrypted_data,
                              key, keylen, iv, ivlen);
        if(ret < 0) {
            pr_err("%s: Failed decrypting the MFG data\n", __FUNCTION__);
            inode_unlock(inode);
            break;
        } else if(ret > info.decrypted_datalen) {
            pr_err("%s: Provided buffer is too small. Length=%d\n",
                   __FUNCTION__, ret);
            ret = -EINVAL;
            inode_unlock(inode);
            break;
        }
        info.decrypted_datalen = ret;
        DEBUG("%s: Decrypted data length: %d\n", __FUNCTION__, info.decrypted_datalen);
#ifdef MFG_DEBUG
        print_hex_dump(KERN_ERR, "Decrypted data: ", DUMP_PREFIX_NONE, 32, 1,
                       info.decrypted_data, info.decrypted_datalen, 0);
#endif
        /* Copy kernel space decrypted_data to userspace decrypted_data */
        if(copy_to_user(decrypted_data, info.decrypted_data,
                        info.decrypted_datalen)) {
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
        /* Free kernelspace memory before loosing track of those pointers */
        kfree(info.encrypted_data);
        kfree(info.decrypted_data);
        kfree(info.cipherkey_name);
        /* Set back pointers in info to userspace pointers before copying info to info_u */
        info.decrypted_data = decrypted_data;
        info.encrypted_data = encrypted_data;
        info.cipherkey_name = cipherkey_name;
        if(copy_to_user(info_u, &info, sizeof(info))) {
            info.decrypted_data = info.encrypted_data = info.cipherkey_name = NULL;
            ret = -EFAULT;
            inode_unlock(inode);
            break;
        }
        /* Nullified those pointers since they now point to userspace memory  */
        info.decrypted_data = info.encrypted_data = info.cipherkey_name = NULL;
        inode_unlock(inode);
        break;
    default:
        pr_err("%s: Unknown command (%u)\n", __FUNCTION__, cmd);
        ret = -EINVAL;
    }
    kfree(encrypted_databin);
    kfree(info.encrypted_data);
    kfree(info.decrypted_data);
    kfree(info.cipherkey_name);
    kfree(keynode_path);

    return ret;
}

static struct file_operations mfg_fops = {
    .owner = THIS_MODULE,
    .unlocked_ioctl = mfg_ioctl,
#ifdef CONFIG_COMPAT
    .compat_ioctl = mfg_ioctl,
#endif
};

/**
 * @ingroup MFG_MODULE_key_in_dtb
 * @brief MFG RIP Key Kernel Initialization
 *
 * Initializes the RIP key by reading it from the kernel device tree.
 *
 * @return 0 on success, an error code on failure.
 */
static int mfg_rip_key_kernel_init(void) {
    struct device_node* node = NULL;
    const __be32* aes_size = NULL;
    const __be32* iv_size = NULL;
    const u8* cipher_key = NULL;
    int ret = 0;
#ifdef MFG_DEBUG
    int i = 0;
#endif

    node = of_find_node_by_path("/rip/cipher");
    if(!node) {
        pr_err("Cipher node not found\n");
        return -ENODEV; // or any other error code
    }

    // Check if all the properties exist
    if(!of_find_property(node, "cipher-key", NULL) ||
       !of_find_property(node, "aes-size", NULL) ||
       !of_find_property(node, "iv-size", NULL)) {
        pr_err("Not all the properties have been found in the node\n");
        of_node_put(node);
        return -EINVAL;
    }

    // Read the properties
    aes_size = of_get_property(node, "aes-size", &ret);
    iv_size = of_get_property(node, "iv-size", &ret);
    cipher_key = of_get_property(node, "cipher-key", NULL);
    if((aes_size == NULL) || (iv_size == NULL) || (cipher_key == NULL)) {
        of_node_put(node);
        return -EINVAL;
    }
    // Release the device tree node
    of_node_put(node);

    // Parse the cipiher_key and split it into the aes-key and the iv-key
    if(0 != mfg_data_alloc_keys(&mfgData, be32_to_cpu(*aes_size), be32_to_cpu(*iv_size))) {
        pr_err("MFG: failed to alloc keys\n");
        return -ENOMEM;
    }

    memcpy(mfgData.mfgKey, cipher_key, mfgData.mfgKey_size);
    memcpy(mfgData.mfgIv, cipher_key + mfgData.mfgKey_size, mfgData.mfgIv_size);

#ifdef MFG_DEBUG
    DEBUG("MFG: decrypted cipher aes key (size = %d): ", mfgData.mfgKey_size);
    DEBUG("MFG: decrypted cipher iv key (size = %d): ", mfgData.mfgIv_size);

    for(i = 0; i < mfgData.mfgKey_size; i++) {
        DEBUG("%02x", mfgData.mfgKey[i]);
    }
    for(i = 0; i < mfgData.mfgIv_size; i++) {
        DEBUG("%02x", mfgData.mfgIv[i]);
    }

    DEBUG("\n");
#endif

    return 0;
}

/**
 * @ingroup MFG_MODULE_key_in_dtb
 * @brief MFG Decryption Module Initialization
 *
 * This function is called when the MFG (Manufacturing) Decryption module is initialized.
 * It performs initialization tasks such as initializing the RIP key, allocating
 * character device region, creating a device class, adding and registering the
 * character device, and creating the corresponding device node in /dev.
 * If any initialization step fails, appropriate error messages are printed,
 * and cleanup is performed before returning an error code.
 *
 * @return 0 on success, an error code on failure.
 */
static int __init mfg_init(void) {
    int retval = 0;

    // Read the rip node from the kernel.dtb.
    retval = mfg_rip_key_kernel_init();
    if(0 != retval) {
        pr_err("MFG: failed to init rip key from kernel.dtb\n");
        return retval;
    }

    /* Allocate the MAJOR number of the device. */
    retval = alloc_chrdev_region(&mfgData.devt, MFG_MINOR, DEVICE_NUMBER, DEVICE_NAME);
    if(retval < 0) {
        pr_err("MFG: alloc_chrdev_region failed (0x%x)\n", retval);
        retval = -ENOMEM;
        goto cleanup_rip_key;
    }

    /* Create /sys/class/mfg in preparation of /dev/mfg */
    mfgData.mfgClass = class_create(THIS_MODULE, CLASS_NAME);
    if(IS_ERR(mfgData.mfgClass)) {
        pr_err("MFG: Failed to create device class\n");
        retval = PTR_ERR(mfgData.mfgClass);
        goto cleanup_chrdev;
    }

    /* Char device registration. Initialize and add the character device. */
    cdev_init(&mfgData.cdev, &mfg_fops);
    mfgData.cdev.owner = THIS_MODULE;
    mfgData.cdev.ops = &mfg_fops;
    retval = cdev_add(&mfgData.cdev, mfgData.devt, DEVICE_NUMBER);
    if(retval) {
        pr_err("MFG: failed to add character device\n");
        goto cleanup_class;
    }

    /* Create /dev/mfg for this char dev. */
    mfgData.mfgDevice = device_create(mfgData.mfgClass, NULL, mfgData.devt, NULL, DEVICE_NAME);
    if(IS_ERR(mfgData.mfgDevice)) {
        printk(KERN_ERR "mfgdecrypt: Failed to create device\n");
        retval = PTR_ERR(mfgData.mfgDevice);
        goto cleanup_cdev;
    }

    pr_info("MFG: SoftAtHome MFG driver has been initialized (major=%d)\n", MAJOR(mfgData.devt));
    return retval;

cleanup_cdev:
    cdev_del(&mfgData.cdev);
cleanup_class:
    class_destroy(mfgData.mfgClass);
cleanup_chrdev:
    unregister_chrdev_region(mfgData.devt, DEVICE_NUMBER);
cleanup_rip_key:
    mfg_data_clear_keys(&mfgData);

    return retval;
}

/**
 * @ingroup MFG_MODULE_key_in_dtb
 * @brief MFG Decryption Module Exit
 *
 * This function cleans up and exits the MFG Decryption module. It destroys the device,
 * character device, and class. Additionally, it unregisters the allocated major number.
 */
static void __exit mfg_exit(void) {
    device_destroy(mfgData.mfgClass, mfgData.devt);
    cdev_del(&mfgData.cdev);
    class_destroy(mfgData.mfgClass);
    unregister_chrdev_region(mfgData.devt, DEVICE_NUMBER);
    mfg_data_clear_keys(&mfgData);

    pr_info("MFG: unregister SoftAtHome MFG driver\n");
}

module_init(mfg_init);
module_exit(mfg_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jonathan Luijsmans");
MODULE_AUTHOR("Paul HENRYS");
MODULE_DESCRIPTION("SoftAtHome driver to decipher MFG secured data");
MODULE_VERSION("1.0");

/**
 * @}
 */
