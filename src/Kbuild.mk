obj-m := $(COMPONENT).o
ccflags-y += -I$(COMPONENT_TOPDIR)/include -I$(COMPONENT_TOPDIR)/include/common/
$(COMPONENT)-y := common/mfg_aes.o common/base64.o common/shared_functions.o

# Both config options can not be enabled, check that it is not the case
ifneq ($(CONFIG_SAH_MFG_DECRYPT_KEY_KERNEL_DTB),)
ifneq ($(CONFIG_SAH_MFG_DECRYPT_KEY_IN_MFG),)
$(error "mfgdecrypt: CONFIG_SAH_MFG_DECRYPT_KEY_KERNEL_DTB and CONFIG_SAH_MFG_DECRYPT_KEY_IN_MFG are enabled. Only one or none enabled is allowed.")
endif
endif

ifneq ($(CONFIG_SAH_MFG_DECRYPT_KEY_KERNEL_DTB),)
	ccflags-y += -DCONFIG_SAH_MFG_DECRYPT_KEY_KERNEL_DTB
	$(COMPONENT)-y += kmod_mfgdecrypt_key_in_dtb.o
else
	ifneq ($(CONFIG_SAH_MFG_DECRYPT_KEY_IN_MFG),)
		ccflags-y += -DCONFIG_SAH_MFG_DECRYPT_KEY_IN_MFG
		$(COMPONENT)-y += kmod_mfgdecrypt_key_in_mfg.o
	else
		$(COMPONENT)-y += kmod_mfgdecrypt.o
	endif
endif
