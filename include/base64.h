/*
 * Base64 encoding/decoding (RFC1341)
 * Copyright (c) 2005, Jouni Malinen <j@w1.fi>
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 */

#ifndef BASE64_H
#define BASE64_H

/**
 * @defgroup base64 base64
 *
 * @brief
 * Module for base64 encoding and decoding.
 */

/**
 * @ingroup base64
 * @brief base64 encoding
 *
 * The encoded data are stored in a newly allocate buffer that should be freed
 * by the caller.
 *
 * @param src     The input buffer to be base64 encoded
 * @param len     The length of the input buffer
 * @param out_len Pointer where to store the length of the encoded data
 *
 * @return a pointer on the encoded data on success or NULL on error.
 */
unsigned char* base64_encode(const unsigned char* src, int len, int* out_len);

/**
 * @ingroup base64
 * @brief base64 decoding
 *
 * The decoded data are stored in a newly allocate buffer that should be freed
 * by the caller.
 *
 * @param src     The input buffer that is base64 encoded
 * @param len     The length of the input buffer
 * @param out_len Pointer where to store the length of the decoded data
 *
 * @return a pointer on the decoded data on success or NULL on error.
 */
unsigned char* base64_decode(const unsigned char* src, int len,
                             int* out_len);

#endif /* BASE64_H */
