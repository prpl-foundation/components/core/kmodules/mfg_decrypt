/****************************************************************************
**
** SPDX-License-Identifier: Dual BSD-2-Clause-Patent AND GPL-2.0-or-later
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** **BSD 2-Clause Patent License**
**
** 1. Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with the distribution.
**
** **GPL 2.0 or later**
**
** Subject to the terms and conditions of the GPL 2.0 or later, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor are granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __MFG_H__
#define __MFG_H__

#include <linux/types.h>

/**
 * @defgroup mfg_decrypt MFG Decrypt header
 * @brief
 * This module defines the mfg_decrypt_info_s struct and IOWR call.
 */

/**
 * @ingroup mfg_decrypt
 * @brief Data structure containing user space data
 *
 * This structure defines all the info that is exchanged between
 * the user and kernel space. It encapsulates the pointers and
 * data length of the encrypted data, decrypted data, and the
 * name of the cipherkey.
 */
typedef struct __attribute__((packed)) mfg_decrypt_info_s {
    union {
        __u8* encrypted_data;
        __u64 encrypted_data_ptr;
    };
    union {
        __u8* decrypted_data;
        __u64 decrypted_data_ptr;
    };
    union {
        __u8* cipherkey_name;
        __u64 cipherkey_name_ptr;
    };
    __u32 encrypted_datalen;
    __u32 decrypted_datalen;
    __u32 cipherkey_namelen;
//TODO: Add info about the encryption type
} mfg_decrypt_info_t;

/* Do not conflict with _IO's defined in include/uapi/linux/random.h and
 * include/uapi/linux/rfkill.h */
/**
 * @ingroup mfg_decrypt
 * @brief The ioctl that can be called from user space.
 *
 * This IOCTL is used for decrypting an encrypted MFG field.
 *
 * The actual operation is performed by passing a data structure of type
 * mfg_decrypt_info_t to or from the IOCTL operation. The mfg_decrypt_info_t struct
 * contains the necessary information for the decryption process.
 */
#define MFG_DECRYPT  _IOWR('R', 0xff, mfg_decrypt_info_t)

#endif /* __MFG_H__ */

/**
 * @}
 */
