/****************************************************************************
**
** SPDX-License-Identifier: Dual BSD-2-Clause-Patent AND GPL-2.0-or-later
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** **BSD 2-Clause Patent License**
**
** 1. Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with the distribution.
**
** **GPL 2.0 or later**
**
** Subject to the terms and conditions of the GPL 2.0 or later, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor are granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SHARED_FUNCTIONS_H
#define SHARED_FUNCTIONS_H

/**
 * @defgroup shared_functions header
 * @brief
 * This file defines shared functions that are used in the different versions of the mfgdecrypt kernel module.
 */

#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/slab.h>

#define DRIVER_NAME     "mfgdecrypt"
#define DEVICE_NAME     "mfg"
#define CLASS_NAME      "mfg"
#define DEVICE_NUMBER   1
#define MFG_MINOR       0

/** Key, IV and tag sizes in bytes */
#define ENC_RIP_KEY_SIZE                48
#define ENC_RIP_IV_SIZE                 12
#define ENC_RIP_TAG_SIZE                16
#define ENC_RIP_KEY_IV_TAG_SIZE         (ENC_RIP_KEY_SIZE + ENC_RIP_IV_SIZE + ENC_RIP_TAG_SIZE)
#define ENC_RIP_KEY_IV_TAG_SIZE_UTF8    (2 * ENC_RIP_KEY_IV_TAG_SIZE)
#define DEC_RIP_KEY_SIZE                32
#define DEC_RIP_IV_SIZE                 16
#define DEC_RIP_KEY_IV_SIZE             (DEC_RIP_KEY_SIZE + DEC_RIP_IV_SIZE)

#ifdef MFG_DEBUG
#define DEBUG(format, args ...)  pr_err(format, ## args)
#else
#define DEBUG(format, args ...)
#endif

/**
 * @ingroup shared_functions
 * @struct mfg_drvdata
 * @brief MFG Decryption Data Structure
 *
 * The mfg_drvdata struct holds data related to MFG Decryption. It includes pointers to
 * the AES key, IV key, and decrypted RIP key. The structure is used to manage the
 * allocation and deallocation of memory for keys during MFG operations.
 */
struct mfg_drvdata {
    struct class* mfgClass;
    struct device* mfgDevice;
    struct cdev cdev;           /* Char device structure */
    dev_t devt;
    u8* mfgKey;
    u32 mfgKey_size;
    u8* mfgIv;
    u32 mfgIv_size;
    void* fdt;                  /* Pointer to a device tree where the RIP keys are stored */
};

/**
 * @ingroup shared_functions
 * @brief MFG Data Clear Keys
 *
 * Clears the keys stored in the MFG Data structure by freeing the allocated memory.
 *
 * @param mfgData The struct wich contains mfg_drvdata.
 */
void mfg_data_clear_keys(struct mfg_drvdata* mfgData);

/**
 * @ingroup shared_functions
 * @brief MFG Data Allocate Keys
 *
 * Allocates memory to store keys in the MFG Data structure. The buffer is appended
 * with an extra byte for the NULL character.
 *
 * In case of an error, both keys are cleared.
 *
 * @param mfgData The struct wich contains mfg_drvdata.
 * @param mfg_key_size The size of the AES MFG key.
 * @param mfg_iv_size The size of the IV MFG key.
 *
 * @return 0 on success, 1 on error.
 */
int mfg_data_alloc_keys(struct mfg_drvdata* mfgData, int mfg_key_size, int mfg_iv_size);

#endif /* SHARED_FUNCTIONS_H */
