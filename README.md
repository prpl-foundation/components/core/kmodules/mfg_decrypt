# mfg_decrypt

[TOC]

# Introduction

This module is used to decrypt ciphered fields of the MFG (Manufacturing) data.
The module is responsible for initializing, handling IOCTL commands, and cleaning up
resources associated with the MFG Decryption functionality.

## Testing
There are currently no unit tests available

## Documentation

### Prerequisites

To generate the documentation, [Doxygen](https://www.doxygen.nl) is required
and already available in the container. In case you want to install this on
your local machine:

    sudo apt-get install doxygen

### Paths
The documentation is split in two parts, starting from the repository path:

    cd ~/components/mfg_decrypt

Here, you can find:

- README.md: this file is used on Gitlab and is the main page for the Doxygen documentation.

The code itself is documented in the approriate header and source files.
Documentation is generated for both the header and source code files.

### Generate documentation

You can generate the documentation by executing the following command:

    cd ~/components/mfg_decrypt
    make doc


The full documentation is available in the `output` directory. You can easily
access the documentation in your browser.

#### Creating .pdf

For creating the pdf, an extra step is necessary.

    cd output/doc/doxy-latex
    make

After using `make`, the .pdf should be available in the directory.
