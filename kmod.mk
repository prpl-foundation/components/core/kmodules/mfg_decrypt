CP = $(shell which cp)
PATCH = $(shell which patch)

# Kernel specifics
INCDIRS += $(HARDCO_KERNEL_DIR)/include

KMOD_RELEASE_PATH_PREFIX = $(ROOT_FS_DIR)
KMOD_INSTALL_PATH_PREFIX = $(STAGINGDIR)
KERNEL_PATH = $(shell cd $(HARDCO_KERNEL_DIR) && find * -maxdepth 2 -name "linux-*" && cd - > /dev/null)
KERNEL_RELEASE = $(shell cat $(HARDCO_KERNEL_DIR)/$(KERNEL_PATH)/include/config/kernel.release)

get_kernel_version:
	$(eval KERNEL_VERSION = $(shell $(MAKE) --silent --no-print-directory -C $(HARDCO_KERNEL_DIR)/$(KERNEL_PATH) kernelversion))
	$(eval KERNEL_VERSION ?= $(shell $(MAKE) --silent --no-print-directory -C $(HARDCO_KERNEL_DIR) kernelversion))

define Module/Compile
	$(MAKE) -C $(HARDCO_KERNEL_DIR) $(KERNEL_MAKE_OPTIONS) M=$(CURDIR) \
		KBUILD_EXTRA_SYMBOLS=$(KBUILD_EXTRA_SYMBOLS) \
		EXTRA_CFLAGS=$(EXTRA_CFLAGS) \
		modules
endef

# $(1) Module install path.
# $(2) Symvers file name without extension.
define Module/Install
	$(MAKE) -C $(HARDCO_KERNEL_DIR) $(KERNEL_MAKE_OPTIONS) M=$(CURDIR) \
		INSTALL_MOD_PATH=$(1) modules_install
	$(INSTALL) -D -p -m 0640 Module.symvers $(1)/Module.symvers.d/$(2).symvers
endef

# $(1) Module release path.
# $(2) Symvers file name without extension.
define Module/Release
	$(MAKE) -C $(HARDCO_KERNEL_DIR) $(KERNEL_MAKE_OPTIONS) M=$(CURDIR) \
		INSTALL_MOD_PATH=$(1) INSTALL_MOD_STRIP=1 modules_install
	$(INSTALL) -D -p -m 0640 Module.symvers $(1)/Module.symvers.d/$(2).symvers
endef

define Module/Clean
	$(MAKE) -C $(HARDCO_KERNEL_DIR) $(KERNEL_MAKE_OPTIONS) M=$(CURDIR) clean
endef

