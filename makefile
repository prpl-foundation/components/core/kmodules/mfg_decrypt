-include $(CONFIGDIR)/components.config

export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export ACLDIR ?= /etc/acl
export DOCDIR ?= $(D)/usr/share/doc/mfgdecrypt
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = mfgdecrypt
export COMPONENT_TOPDIR = $(CURDIR)

compile:
	$(MAKE) -C $(LINUX_DIR) modules M=$(BUILD_DIR)/src

clean:
	$(MAKE) -C src clean

install:
	$(INSTALL) -D -p -m 644 include/mfg_decrypt.h $(D)/include/mfg_decrypt.h
	$(INSTALL) -D -p -m 755 files/etc/init.d/mfgdecrypt $(D)/etc/init.d/mfgdecrypt

.PHONY: compile clean install
